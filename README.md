# LocalWeather

A simple Android application. It gets current weather and forecast for your location.

![](https://gitlab.com/haluzpav/localweather/raw/master/screens/Screenshot_1563726314.png) 
![](https://gitlab.com/haluzpav/localweather/raw/master/screens/Screenshot_1563726320.png) 

External services used:

- https://openweathermap.org/
- https://www.mapbox.com/
