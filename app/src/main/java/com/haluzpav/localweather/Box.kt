package com.haluzpav.localweather

/**
 * The new standard {@link Result} can't be empty... This {@link Box} is heavily inspired by it.
 */
class Box<T> internal constructor(
    private val value: Any?
) {

    companion object {
        fun <T> empty(): Box<T> = Box(null)
        fun <T> with(value: T?): Box<T> = Box(value)
        fun <T> brokenBy(throwable: Throwable): Box<T> = Box(throwable)
    }

    val isEmpty: Boolean get() = value == null
    val isFull: Boolean get() = !isEmpty && !isBroken
    val isBroken: Boolean get() = value is Throwable

    @Suppress("UNCHECKED_CAST")
    val valueOrNull: T?
        get() = if (isFull) value as T else null

    fun onEmpty(action: () -> Unit): Box<T> {
        if (isEmpty) action()
        return this
    }

    fun onFull(action: (value: T) -> Unit): Box<T> {
        @Suppress("UNCHECKED_CAST")
        if (isFull) action(value as T)
        return this
    }

    fun onBroken(action: (value: Throwable) -> Unit): Box<T> {
        if (isBroken) action(value as Throwable)
        return this
    }

}