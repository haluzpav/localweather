package com.haluzpav.localweather.detail

import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.haluzpav.localweather.BaseActivity
import com.haluzpav.localweather.R
import com.haluzpav.localweather.getIcon
import com.haluzpav.localweather.model.forecast.City
import com.haluzpav.localweather.model.forecast.Forecast
import com.haluzpav.localweather.model.forecast.Sample
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.item_detail.view.*
import kotlinx.android.synthetic.main.progress.*
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneOffset
import org.threeten.bp.format.DateTimeFormatter


class DetailActivity : BaseActivity<DetailViewModel>() {

    override val layout: Int
        get() = R.layout.activity_detail

    private lateinit var location: Location

    companion object {
        const val EXTRA_LOCATION = "lokejšn"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        // TODO put to model?
        location = intent.getParcelableExtra(EXTRA_LOCATION)

        recycler.apply {
            layoutManager =
                GridLayoutManager(context, resources.getInteger(R.integer.detail_columns), RecyclerView.VERTICAL, false)
            adapter = Adapter()
        }

        model = ViewModelProviders.of(this).get(DetailViewModel::class.java)
        model.getForecast().observe(this, Observer { forecast ->
            forecast.onEmpty {
                setupLoading(R.string.loading_forecast)
                model.loadForecast(location)
            }.onFull {
                setupForecast(it)
            }.onBroken {
                setupNoData()
                toast(it.localizedMessage)
            }
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun setupLoading(loadingText: Int) {
        super.setupLoading(loadingText)
        content.visibility = View.INVISIBLE
    }

    override fun setupNoData() {
        super.setupNoData()
        content.visibility = View.INVISIBLE
    }

    private fun setupForecast(forecast: Forecast) {
        progressView.visibility = View.INVISIBLE
        content.visibility = View.VISIBLE
        place.text = forecast.city.name
        recycler.adapter?.notifyDataSetChanged()
    }

    private inner class Adapter : RecyclerView.Adapter<ItemHolder>() {
        private val forecast
            get() = model.getForecast().value?.valueOrNull
        private val city
            get() = forecast!!.city
        private val samples
            get() = forecast!!.list

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_detail, parent, false)
            return ItemHolder(view)
        }

        override fun getItemCount(): Int = if (forecast == null) 0 else samples.size

        override fun onBindViewHolder(holder: ItemHolder, position: Int) {
            if (forecast == null) return
            holder.setup(city, samples[position])
        }

    }

    private class ItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        companion object {
            // "Tue 9:00" // TODO locale format
            val formatter = DateTimeFormatter.ofPattern("EEE H:mm")!!
        }

        fun setup(city: City, sample: Sample) {
            val dateTime = LocalDateTime.ofEpochSecond(sample.dt, 0, ZoneOffset.ofHours(city.timezone / 60 / 60))
            itemView.day.text = dateTime.format(formatter)
            itemView.image.setImageResource(itemView.context.getIcon(sample.weather[0].icon))
            itemView.temperature.text = itemView.context.getString(R.string.temperature, sample.main.temp)
        }

    }

}