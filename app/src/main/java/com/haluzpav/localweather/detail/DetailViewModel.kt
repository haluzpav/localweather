package com.haluzpav.localweather.detail

import android.location.Location
import androidx.lifecycle.LiveData
import com.haluzpav.localweather.BaseViewModel
import com.haluzpav.localweather.Box
import com.haluzpav.localweather.BoxedLiveData
import com.haluzpav.localweather.fetchInto
import com.haluzpav.localweather.model.forecast.Forecast

class DetailViewModel : BaseViewModel() {

    private val forecast: BoxedLiveData<Forecast> by lazy {
        BoxedLiveData<Forecast>()
    }

    fun getForecast(): LiveData<Box<Forecast>> = forecast

    fun loadForecast(location: Location) {
        repository.service.forecast(location.latitude, location.longitude).fetchInto(forecast)
    }
}