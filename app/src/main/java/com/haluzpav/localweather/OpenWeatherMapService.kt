package com.haluzpav.localweather

import com.haluzpav.localweather.model.currentweather.CurrentWeather
import com.haluzpav.localweather.model.forecast.Forecast
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface OpenWeatherMapService {

    @GET("weather")
    fun currentWeather(
        @Query("q") city: String
    ): Call<CurrentWeather>

    @GET("weather")
    fun currentWeather(
        @Query("lat") latitude: Double,
        @Query("lon") longitude: Double
    ): Call<CurrentWeather>

    @GET("forecast")
    fun forecast(
        @Query("lat") latitude: Double,
        @Query("lon") longitude: Double
    ): Call<Forecast>

}