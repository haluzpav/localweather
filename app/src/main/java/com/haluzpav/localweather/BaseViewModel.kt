package com.haluzpav.localweather

import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel() {

    protected val repository = OpenWeatherMapRepository()

}