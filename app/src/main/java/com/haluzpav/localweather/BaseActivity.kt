package com.haluzpav.localweather

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import kotlinx.android.synthetic.main.progress.*
import kotlinx.android.synthetic.main.toolbar.*

abstract class BaseActivity<MODEL : ViewModel> : AppCompatActivity() {

    @get:LayoutRes
    protected abstract val layout: Int
    protected lateinit var model: MODEL

    protected open fun onPreCreate() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onPreCreate()
        // TODO put toolbar and progressView to a single layout
        setContentView(layout)
        setSupportActionBar(toolbar)
        // TODO init model?
    }

    protected open fun setupLoading(@StringRes loadingText: Int) {
        progressView.visibility = View.VISIBLE
        progressBar.visibility = View.VISIBLE
        progressTextView.visibility = View.VISIBLE
        progressTextView.setText(loadingText)
    }

    protected open fun setupNoData() {
        progressView.visibility = View.VISIBLE
        progressBar.visibility = View.GONE
        progressTextView.visibility = View.VISIBLE
        progressTextView.setText(R.string.no_data)
    }

    protected fun toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    protected fun toast(@StringRes message: Int) {
        toast(getString(message))
    }

}