package com.haluzpav.localweather

import android.content.Context
import androidx.annotation.DrawableRes
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

@DrawableRes
fun Context.getIcon(name: String): Int = resources.getIdentifier("ic_$name", "drawable", packageName)

fun <T> Call<T>.fetchInto(liveData: BoxedLiveData<T>) {
    enqueue(object : Callback<T> {
        override fun onFailure(call: Call<T>, t: Throwable) {
            liveData.value = Box.brokenBy(t)
        }

        override fun onResponse(call: Call<T>, response: Response<T>) {
            liveData.value = Box.with(response.body()!!)
        }
    })
}
