package com.haluzpav.localweather.main

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.annotation.StringRes
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.haluzpav.localweather.*
import com.haluzpav.localweather.detail.DetailActivity
import com.haluzpav.localweather.model.currentweather.CurrentWeather
import com.mapbox.android.core.location.LocationEngineCallback
import com.mapbox.android.core.location.LocationEngineResult
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.constants.MapboxConstants
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.Style
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.progress.*


class MainActivity : BaseActivity<MainViewModel>(), PermissionsListener {

    override val layout: Int
        get() = R.layout.activity_main

    private lateinit var permissionsManager: PermissionsManager
    private lateinit var mapboxMap: MapboxMap

    private val defaultCameraPosition
        get() = CameraPosition.Builder()
            .target(LatLng(model.lastLocation.value!!.valueOrNull))
            .zoom(DEFAULT_ZOOM)
            .tilt(MapboxConstants.MINIMUM_TILT)
            .bearing(0.0)
            .build()

    companion object {
        private const val DEFAULT_ZOOM = 8.0
    }

    override fun onPreCreate() {
        super.onPreCreate()
        Mapbox.getInstance(applicationContext, BuildConfig.MAP_TOKEN)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        model = ViewModelProviders.of(this).get(MainViewModel::class.java)
        model.lastLocation.observe(this, Observer { box ->
            box.onEmpty {
                setupLoading(R.string.loading_location)
            }.onFull {
                setupLoading(R.string.loading_weather)
                model.loadCurrentWeather(it)
            }.onBroken {
                setupNoData()
                toast(it.localizedMessage)
            }
        })
        model.getCurrentWeather().observe(this, Observer { box ->
            box.onEmpty {
            }.onFull {
                setupWeather(it)
            }.onBroken {
                setupNoData()
                toast(it.localizedMessage)
            }
        })

        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync { mapboxMap ->
            this.mapboxMap = mapboxMap
            mapboxMap.setStyle(Style.DARK) {
                enableLocationComponent(it)
                trySetDefaultCameraPosition()
            }
        }
        weatherView.setOnClickListener {
            openDetail()
        }
    }

    private fun openDetail() {
        val intent = Intent(this, DetailActivity::class.java).apply {
            putExtra(DetailActivity.EXTRA_LOCATION, model.lastLocation.value?.valueOrNull)
        }
        startActivity(intent)
    }

    override fun setupLoading(@StringRes loadingText: Int) {
        super.setupLoading(loadingText)
        mapView.visibility = View.INVISIBLE
        weatherView.visibility = View.GONE
    }

    override fun setupNoData() {
        super.setupNoData()
        mapView.visibility = View.INVISIBLE
        weatherView.visibility = View.GONE
    }

    private fun setupWeather(weather: CurrentWeather) {
        mapView.visibility = View.VISIBLE
        trySetDefaultCameraPosition()
        progressView.visibility = View.INVISIBLE
        weatherView.visibility = View.VISIBLE
        image.setImageResource(getIcon(weather.weather[0].icon))
        temperature.text = getString(R.string.temperature, weather.main.temp)
        pressure.text = getString(R.string.pressure, weather.main.pressure)
        humidity.text = getString(R.string.humidity, weather.main.humidity)
        wind.text = getString(R.string.wind, weather.wind.speed)
    }

    private fun trySetDefaultCameraPosition() {
        if (!::mapboxMap.isInitialized) {
            return
        } else if (model.lastLocation.value?.isFull != true) {
            return
        }
        // enforce correct position
        mapboxMap.cameraPosition = defaultCameraPosition
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?) = when (item?.itemId) {
        R.id.refresh -> {
            model.clear()
            fetchLastLocation()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    @SuppressLint("MissingPermission")
    private fun enableLocationComponent(loadedMapStyle: Style) {
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            val locationComponent = mapboxMap.locationComponent
            locationComponent.activateLocationComponent(
                LocationComponentActivationOptions.builder(this, loadedMapStyle).build()
            )
            locationComponent.isLocationComponentEnabled = true
            locationComponent.cameraMode = CameraMode.NONE
            locationComponent.renderMode = RenderMode.NORMAL
            // locationComponent.zoomWhileTracking(DEFAULT_ZOOM) // not reliable
            fetchLastLocation()
        } else {
            permissionsManager = PermissionsManager(this)
            permissionsManager.requestLocationPermissions(this)
        }
    }

    // TODO move to model somehow?
    @SuppressLint("MissingPermission")
    private fun fetchLastLocation() {
        mapboxMap.locationComponent.locationEngine!!.getLastLocation(object :
            LocationEngineCallback<LocationEngineResult> {
            override fun onSuccess(result: LocationEngineResult?) {
                val lastLocation = result?.lastLocation
                model.lastLocation.value = Box.with(lastLocation)
                if (lastLocation == null) {
                    // shouldn't happen too often
                    fetchLastLocation()
                }
            }

            override fun onFailure(exception: Exception) {
                exception.printStackTrace()
                model.lastLocation.value = Box.brokenBy(exception)
                toast(exception.localizedMessage)
            }
        })
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onExplanationNeeded(permissionsToExplain: List<String>) {
        toast(R.string.user_location_permission_explanation)
    }

    override fun onPermissionResult(granted: Boolean) {
        if (granted) {
            mapboxMap.getStyle { style -> enableLocationComponent(style) }
        } else {
            toast(R.string.user_location_permission_not_granted)
            finish()
        }
    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)
        mapView.onSaveInstanceState(outState)
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView.onDestroy()
    }
}
