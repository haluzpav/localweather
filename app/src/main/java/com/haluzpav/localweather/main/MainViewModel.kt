package com.haluzpav.localweather.main

import android.location.Location
import androidx.lifecycle.LiveData
import com.haluzpav.localweather.BaseViewModel
import com.haluzpav.localweather.Box
import com.haluzpav.localweather.BoxedLiveData
import com.haluzpav.localweather.fetchInto
import com.haluzpav.localweather.model.currentweather.CurrentWeather

class MainViewModel : BaseViewModel() {

    val lastLocation: BoxedLiveData<Location> by lazy {
        BoxedLiveData<Location>()
    }
    private val currentWeather: BoxedLiveData<CurrentWeather> by lazy {
        BoxedLiveData<CurrentWeather>()
    }

    fun getCurrentWeather(): LiveData<Box<CurrentWeather>> = currentWeather

    fun loadCurrentWeather(location: Location) {
        repository.service.currentWeather(location.latitude, location.longitude).fetchInto(currentWeather)
    }

    fun clear() {
        lastLocation.value = Box.empty()
        currentWeather.value = Box.empty()
    }

}