package com.haluzpav.localweather

import androidx.lifecycle.MutableLiveData

class BoxedLiveData<T> : MutableLiveData<Box<T>>() {

    init {
        value = Box.empty()
    }

}