package com.haluzpav.localweather.model.forecast

import com.google.gson.annotations.SerializedName
import com.haluzpav.localweather.model.Coord

data class City(

    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("coord") val coord: Coord,
    @SerializedName("country") val country: String,
    @SerializedName("timezone") val timezone: Int
)