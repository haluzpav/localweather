package com.haluzpav.localweather.model.forecast

import com.google.gson.annotations.SerializedName
import com.haluzpav.localweather.model.Clouds
import com.haluzpav.localweather.model.Weather
import com.haluzpav.localweather.model.Wind

data class Sample(

    @SerializedName("dt") val dt: Long,
    @SerializedName("main") val main: Main,
    @SerializedName("weather") val weather: List<Weather>,
    @SerializedName("clouds") val clouds: Clouds,
    @SerializedName("wind") val wind: Wind,
    @SerializedName("sys") val sys: Sys,
    @SerializedName("dt_txt") val dt_txt: String
)