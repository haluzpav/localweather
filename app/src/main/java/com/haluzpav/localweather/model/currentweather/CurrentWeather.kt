package com.haluzpav.localweather.model.currentweather

import com.google.gson.annotations.SerializedName
import com.haluzpav.localweather.model.Clouds
import com.haluzpav.localweather.model.Coord
import com.haluzpav.localweather.model.Weather
import com.haluzpav.localweather.model.Wind

data class CurrentWeather(

    @SerializedName("coord") val coord: Coord,
    @SerializedName("weather") val weather: List<Weather>, // always one item??
    @SerializedName("base") val base: String,
    @SerializedName("main") val main: Main,
    @SerializedName("visibility") val visibility: Int,
    @SerializedName("wind") val wind: Wind,
    @SerializedName("clouds") val clouds: Clouds,
    @SerializedName("dt") val dt: Long,
    @SerializedName("sys") val sys: Sys,
    @SerializedName("timezone") val timezone: Int,
    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("cod") val cod: Int
)