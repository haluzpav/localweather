package com.haluzpav.localweather.model.forecast

import com.google.gson.annotations.SerializedName

data class Forecast(

    @SerializedName("city") val city: City,
    @SerializedName("cod") val cod: Int,
    @SerializedName("message") val message: Double,
    @SerializedName("cnt") val cnt: Int,
    @SerializedName("list") val list: List<Sample>
)