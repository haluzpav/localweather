package com.haluzpav.localweather

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class OpenWeatherMapRepository {

    private val standardParamsInjector = { chain: Interceptor.Chain ->
        var request = chain.request()
        val url = request.url.newBuilder()
            .addQueryParameter("units", "metric")
            .addQueryParameter("appid", BuildConfig.WEATHER_KEY)
            .build()
        request = request.newBuilder().url(url).build()
        chain.proceed(request)
    }

    private val loggingInterceptor = HttpLoggingInterceptor().apply {
        level = HttpLoggingInterceptor.Level.BODY
    }

    private val client = OkHttpClient.Builder()
        .addInterceptor(standardParamsInjector)
        .addInterceptor(loggingInterceptor)
        .build()

    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(BuildConfig.WEATHER_URL_BASE)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val service: OpenWeatherMapService = retrofit.create(OpenWeatherMapService::class.java)

}