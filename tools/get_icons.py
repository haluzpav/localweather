import urllib.request

url_mask = r'http://openweathermap.org/img/wn/{}@2x.png'
file_mask = 'ic_{}.png'
icons = [
    '01d',
    '01n',
    '02d',
    '02n',
    '03d',
    '03n',
    '04d',
    '04n',
    '09d',
    '09n',
    '10d',
    '10n',
    '11d',
    '11n',
    '13d',
    '13n',
    '50d',
    '50n',
]
for icon in icons:
    url = url_mask.format(icon)
    print(url)
    urllib.request.urlretrieve(url, file_mask.format(icon))
